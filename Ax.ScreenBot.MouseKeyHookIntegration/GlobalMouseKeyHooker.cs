﻿using Gma.System.MouseKeyHook;
using System;
using System.Windows.Forms;

namespace Ax.ScreenBot.MouseKeyHookIntegration
{
    /// <summary> Watches for key and mousestrokes done by the user </summary>
    public sealed class GlobalMouseKeyHooker
    {
        public event EventHandler<Keys> KeyPressed;
        public event EventHandler ShiftMouseClicked;

        private IKeyboardMouseEvents _globalHook;

        public void StartWatching()
        {
            StopWatching();

            // Subscribe
            _globalHook = Hook.GlobalEvents();
            _globalHook.KeyDown += OnKeyDown;
            _globalHook.MouseClick += OnMouseClick;
            _globalHook.MouseDoubleClick += OnMouseDoubleClick;
        }

        public void StopWatching()
        {
            if (_globalHook == null)
                return;

            _globalHook.KeyDown -= OnKeyDown;
            _globalHook.MouseClick -= OnMouseClick;
            _globalHook.MouseDoubleClick -= OnMouseDoubleClick;
            _globalHook.Dispose();
            _globalHook = null;
        }

        private void OnKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            KeyPressed?.Invoke(null, e.KeyCode);
        }

        private void OnMouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (System.Windows.Input.Keyboard.Modifiers == System.Windows.Input.ModifierKeys.Shift)
            {
                ShiftMouseClicked?.Invoke(null, EventArgs.Empty);
            }
        }

        private void OnMouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (System.Windows.Input.Keyboard.Modifiers == System.Windows.Input.ModifierKeys.Shift)
            {
                ShiftMouseClicked?.Invoke(null, EventArgs.Empty);
            }
        }

        ~GlobalMouseKeyHooker()
        {
            StopWatching();
        }
    }
}