﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Ax.ScreenBot.Gui.IoHandling.File;
using Action = Ax.ScreenBot.Model.Actions.Action;

namespace Ax.ScreenBot.Gui.Commands
{
    internal sealed class LoadFileCommandHandler
    {
        private readonly ICollection<Action> _storedActions;

        public LoadFileCommandHandler(ICollection<Action> storedActions)
        {
            _storedActions = storedActions ?? throw new ArgumentNullException(nameof(storedActions));
        }
        
        public void Execute()
        {
            var win = new Microsoft.Win32.OpenFileDialog();
            win.InitialDirectory = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            if (win.ShowDialog() == true)
            {
                LoadFileContent(win.FileName);
            }
        }

        private void LoadFileContent(string path)
        {
            _storedActions.Clear();
            foreach (var item in FileOperator.Deserialize(path))
                _storedActions.Add(item);
        }
    }
}