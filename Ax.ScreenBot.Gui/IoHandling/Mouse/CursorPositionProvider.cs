using Ax.ScreenBot.Gui.Properties;
using System;
using System.Windows.Threading;
using Microsoft.Toolkit.Mvvm.ComponentModel;

namespace Ax.ScreenBot.Gui.IoHandling.Mouse
{
    internal sealed class CursorPositionProvider : ObservableObject
    {
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        
        public CursorPositionProvider()
        {
            DispatcherTimer cursorPollTimer = new DispatcherTimer();

            cursorPollTimer.Interval = new TimeSpan(0, 0, 0, 0, Settings.Default.CursorPositionProvider_CheckFrequencyMilliseconds);
            cursorPollTimer.Tick += (_, _) =>
            {
                var pos  = WinInputUtilities.MouseHelper.GetCursorPosition();
                PositionX = pos.x;
                PositionY = pos.y;
                OnPropertyChanged(nameof(PositionX));
                OnPropertyChanged(nameof(PositionY));
            };
            
            cursorPollTimer.Start();
        }
    }
}