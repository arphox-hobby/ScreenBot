﻿using System.Linq;
using System.Windows;
using System.Windows.Input;
using Ax.ScreenBot.Gui.IoHandling.File;

namespace Ax.ScreenBot.Gui
{
    public partial class MainWindow
    {
        private readonly ViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            _viewModel = (ViewModel)DataContext;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Application.Current.Shutdown();
        }

        private void ListBox_StoredActions_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && listBox_StoredActions.SelectedItems.Count > 0)
            {
                var selectedActions = listBox_StoredActions.SelectedItems.Cast<Model.Actions.Action>().ToList();
                _viewModel.RemoveActions(selectedActions);

                if (listBox_StoredActions.SelectedIndex > -1)
                    listBox_StoredActions.SelectedIndex--;
                else
                    listBox_StoredActions.SelectedIndex = 0;
            }
        }

        private void ListBox_StoredActions_DropInto(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] data = (string[])e.Data.GetData(DataFormats.FileDrop);
                string path = data[0]; //Single File drop
                _viewModel.StoredActions.Clear();
                foreach (var item in FileOperator.Deserialize(path))
                    _viewModel.StoredActions.Add(item);
            }
        }
    }
}