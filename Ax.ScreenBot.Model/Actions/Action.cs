using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Ax.ScreenBot.Model.Actions
{
    [XmlInclude(typeof(ClickAction))]
    [XmlInclude(typeof(WaitAction))]
    [XmlInclude(typeof(JumpAction))]
    public abstract class Action
    {
        [XmlElement(nameof(WaitTimeAfterMs))]
        public uint WaitTimeAfterMs { get; set; }

        protected Action() { }

        protected Action(uint waitTimeAfterMs)
        {
            WaitTimeAfterMs = waitTimeAfterMs;
        }

        public virtual async Task PerformAsync(CancellationToken cancellationToken)
        {
            await Task.Delay((int)WaitTimeAfterMs, cancellationToken);
        }
    }
}